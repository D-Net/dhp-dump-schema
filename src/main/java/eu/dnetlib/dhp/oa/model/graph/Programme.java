
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To store information about the ec programme for the project. It has the following parameters: - private String code
 * to store the code of the programme - private String description to store the description of the programme
 */
public class Programme implements Serializable {
	@JsonSchema(description = "The code of the programme")
	private String code;

	@JsonSchema(description = "The description of the programme")
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static Programme newInstance(String code, String description) {
		Programme p = new Programme();
		p.code = code;
		p.description = description;
		return p;
	}
}
