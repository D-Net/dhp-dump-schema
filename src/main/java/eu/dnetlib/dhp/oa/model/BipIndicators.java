
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author miriam.baglioni
 * @Date 07/11/22
 */
public class BipIndicators implements Serializable {
	private Double citationCount;
	private Double influence;
	private Double popularity;
	private Double impulse;
	private String citationClass;
	private String influenceClass;
	private String impulseClass;
	private String popularityClass;


	public String getPopularityClass() {
		return popularityClass;
	}

	public void setPopularityClass(String popularityClass) {
		this.popularityClass = popularityClass;
	}

	public Double getCitationCount() {
		return citationCount;
	}

	public void setCitationCount(Double citationCount) {
		this.citationCount = citationCount;
	}

	public Double getInfluence() {
		return influence;
	}

	public void setInfluence(Double influence) {
		this.influence = influence;
	}

	public Double getPopularity() {
		return popularity;
	}

	public void setPopularity(Double popularity) {
		this.popularity = popularity;
	}

	public Double getImpulse() {
		return impulse;
	}

	public void setImpulse(Double impulse) {
		this.impulse = impulse;
	}

	public String getCitationClass() {
		return citationClass;
	}

	public void setCitationClass(String citationClass) {
		this.citationClass = citationClass;
	}

	public String getInfluenceClass() {
		return influenceClass;
	}

	public void setInfluenceClass(String influenceClass) {
		this.influenceClass = influenceClass;
	}

	public String getImpulseClass() {
		return impulseClass;
	}

	public void setImpulseClass(String impulseClass) {
		this.impulseClass = impulseClass;
	}
}
