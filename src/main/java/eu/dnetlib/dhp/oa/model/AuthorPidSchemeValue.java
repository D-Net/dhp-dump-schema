
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class AuthorPidSchemeValue implements Serializable {

	@JsonSchema(description = "The author's pid scheme.  OpenAIRE currently supports 'ORCID'")
	private String scheme;

	@JsonSchema(description = "The author's pid value in that scheme (i.e. 0000-1111-2222-3333)")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static AuthorPidSchemeValue newInstance(String scheme, String value) {
		AuthorPidSchemeValue cf = new AuthorPidSchemeValue();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}
}
