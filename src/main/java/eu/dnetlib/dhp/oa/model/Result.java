
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To represent the dumped result. It will be extended in the dump for Research Communities - Research
 * Initiative/Infrastructures. It has the following parameters:
 * - author of type
 * List<eu.dnetlib.dhpschema.dump.oaf.Author> to describe the authors of a result. For each author in the result
 * represented in the internal model one author in the esternal model is produced.
 * - type of type String to represent
 * the category of the result. Possible values are publication, dataset, software, other. It corresponds to
 * resulttype.classname of the dumped result
 * - language of type eu.dnetlib.dhp.schema.dump.oaf.Language to store
 * information about the language of the result. It is dumped as - code corresponds to language.classid - value
 * corresponds to language.classname
 * - country of type List<eu.dnetlib.dhp.schema.dump.oaf.Country> to store the country
 * list to which the result is associated. For each country in the result respresented in the internal model one country
 * in the external model is produces - subjects of type List<eu.dnetlib.dhp.dump.oaf.Subject> to store the subjects for
 * the result. For each subject in the result represented in the internal model one subject in the external model is
 * produced - maintitle of type String to store the main title of the result. It corresponds to the value of the first
 * title in the resul to be dumped having classid equals to "main title" - subtitle of type String to store the subtitle
 * of the result. It corresponds to the value of the first title in the resul to be dumped having classid equals to
 * "subtitle" - description of type List<String> to store the description of the result. It corresponds to the list of
 * description.value in the result represented in the internal model - publicationdate of type String to store the
 * pubblication date. It corresponds to dateofacceptance.value in the result represented in the internal model -
 * publisher of type String to store information about the publisher. It corresponds to publisher.value of the result
 * represented in the intrenal model - embargoenddate of type String to store the embargo end date. It corresponds to
 * embargoenddate.value of the result represented in the internal model - source of type List<String> See definition of
 * Dublin Core field dc:source. It corresponds to the list of source.value in the result represented in the internal
 * model - format of type List<String> It corresponds to the list of format.value in the result represented in the
 * internal model - contributor of type List<String> to represent contributors for this result. It corresponds to the
 * list of contributor.value in the result represented in the internal model - coverage of type String. It corresponds
 * to the list of coverage.value in the result represented in the internal model - bestaccessright of type
 * eu.dnetlib.dhp.schema.dump.oaf.AccessRight to store informatin about the openest access right associated to the
 * manifestations of this research results. It corresponds to the same parameter in the result represented in the
 * internal model - container of type eu.dnetlib.dhp.schema/dump.oaf.Container (only for result of type publication). It
 * corresponds to the parameter journal of the result represented in the internal model - documentationUrl of type
 * List<String> (only for results of type software) to store the URLs to the software documentation. It corresponds to
 * the list of documentationUrl.value of the result represented in the internal model - codeRepositoryUrl of type String
 * (only for results of type software) to store the URL to the repository with the source code. It corresponds to
 * codeRepositoryUrl.value of the result represented in the internal model - programmingLanguage of type String (only
 * for results of type software) to store the programming language. It corresponds to programmingLanguaga.classid of the
 * result represented in the internal model - contactperson of type List<String> (only for results of type other) to
 * store the contact person for this result. It corresponds to the list of contactperson.value of the result represented
 * in the internal model - contactgroup of type List<String> (only for results of type other) to store the information
 * for the contact group. It corresponds to the list of contactgroup.value of the result represented in the internal
 * model - tool of type List<String> (only fro results of type other) to store information about tool useful for the
 * interpretation and/or re-used of the research product. It corresponds to the list of tool.value in the result
 * represented in the internal modelt - size of type String (only for results of type dataset) to store the size of the
 * dataset. It corresponds to size.value in the result represented in the internal model - version of type String (only
 * for results of type dataset) to store the version. It corresponds to version.value of the result represented in the
 * internal model - geolocation fo type List<eu.dnetlib.dhp.schema.dump.oaf.GeoLocation> (only for results of type
 * dataset) to store geolocation information. For each geolocation element in the result represented in the internal
 * model a GeoLocation in the external model il produced - id of type String to store the OpenAIRE id of the result. It
 * corresponds to the id of the result represented in the internal model - originalId of type List<String> to store the
 * original ids of the result. It corresponds to the originalId of the result represented in the internal model - pid of
 * type List<eu.dnetlib.dhp.schema.dump.oaf.ControlledField> to store the persistent identifiers for the result. For
 * each pid in the results represented in the internal model one pid in the external model is produced. The value
 * correspondence is: - scheme corresponds to pid.qualifier.classid of the result represented in the internal model -
 * value corresponds to the pid.value of the result represented in the internal model - dateofcollection of type String
 * to store information about the time OpenAIRE collected the record. It corresponds to dateofcollection of the result
 * represented in the internal model - lasteupdatetimestamp of type String to store the timestamp of the last update of
 * the record. It corresponds to lastupdatetimestamp of the resord represented in the internal model
 *
 */
public class Result implements Serializable {

	private List<Author> authors;

	// resulttype allows subclassing results into publications | datasets | software

	@JsonProperty("isGreen")
	@JsonSchema(description = "True if the result is green Open Access")
	private Boolean isGreen;

	@JsonSchema(description = "The Open Access Color of the publication")
	private OpenAccessColor openAccessColor;

	@JsonProperty("isInDiamondJournal")
	@JsonSchema(description = "True if the result is published in a Diamond Journal")
	private Boolean isInDiamondJournal;

	@JsonSchema(description = "True if the result is outcome of a project")
	private Boolean publiclyFunded;

	public Boolean getIsGreen() {
		return isGreen;
	}

	public void setIsGreen(Boolean green) {
		isGreen = green;
	}

	public OpenAccessColor getOpenAccessColor() {
		return openAccessColor;
	}

	public void setOpenAccessColor(OpenAccessColor openAccessColor) {
		this.openAccessColor = openAccessColor;
	}

	public Boolean getIsInDiamondJournal() {
		return isInDiamondJournal;
	}

	public void setIsInDiamondJournal(Boolean inDiamondJournal) {
		isInDiamondJournal = inDiamondJournal;
	}

	public Boolean getPubliclyFunded() {
		return publiclyFunded;
	}

	public void setPubliclyFunded(Boolean publiclyFunded) {
		this.publiclyFunded = publiclyFunded;
	}

	@JsonSchema(
			description = "Type of the result: one of 'publication', 'dataset', 'software', 'other' (see also https://api.openaire.eu/vocabularies/dnet:result_typologies)")
	private String type; // resulttype

	// common fields
	private Language language;

	@JsonSchema(description = "The list of countries associated to this result")
	private List<ResultCountry> countries;

	@JsonSchema(description = "Keywords associated to the result")
	private List<Subject> subjects;

	@JsonSchema(
			description = "A name or title by which a scientific result is known. May be the title of a publication, of a dataset or the name of a piece of software.")
	private String mainTitle;

	@JsonSchema(description = "Explanatory or alternative name by which a scientific result is known.")
	private String subTitle;

	private List<String> descriptions;

	@JsonSchema(
			description = "Main date of the research product: typically the publication or issued date. In case of a research result with different versions with different dates, the date of the result is selected as the most frequent well-formatted date. If not available, then the most recent and complete date among those that are well-formatted. For statistics, the year is extracted and the result is counted only among the result of that year. Example: Pre-print date: 2019-02-03, Article date provided by repository: 2020-02, Article date provided by Crossref: 2020, OpenAIRE will set as date 2019-02-03, because it’s the most recent among the complete and well-formed dates. If then the repository updates the metadata and set a complete date (e.g. 2020-02-12), then this will be the new date for the result because it becomes the most recent most complete date. However, if OpenAIRE then collects the pre-print from another repository with date 2019-02-03, then this will be the “winning date” because it becomes the most frequent well-formatted date.")
	private String publicationDate; // dateofacceptance;

	@JsonSchema(
			description = "The name of the entity that holds, archives, publishes prints, distributes, releases, issues, or produces the resource.")
	private String publisher;

	@JsonSchema(description = "Date when the embargo ends and this result turns Open Access")
	private String embargoEndDate;

	@JsonSchema(description = "See definition of Dublin Core field dc:source")
	private List<String> sources;

	private List<String> formats;

	@JsonSchema(description = "Contributors for the result")
	private List<String> contributors;

	private List<String> coverages;

	@JsonSchema(description = "The openest of the access rights of this result.")
	private BestAccessRight bestAccessRight;

	@JsonSchema(
			description = "Container has information about the conference or journal where the result has been presented or published")
	private Container container;// Journal

	@JsonSchema(description = "Only for results with type 'software': URL to the software documentation")
	private List<String> documentationUrls; // software

	@JsonSchema(description = "Only for results with type 'software': the URL to the repository with the source code")
	private String codeRepositoryUrl; // software

	@JsonSchema(description = "Only for results with type 'software': the programming language")
	private String programmingLanguage; // software

	@JsonSchema(
			description = "Only for results with type 'software': Information on the person responsible for providing further information regarding the resource")
	private List<String> contactPeople; // orp

	@JsonSchema(
			description = "Only for results with type 'software': Information on the group responsible for providing further information regarding the resource")
	private List<String> contactGroups; // orp

	@JsonSchema(
			description = "Only for results with type 'other': tool useful for the interpretation and/or re-used of the research product")
	private List<String> tools; // orp

	@JsonSchema(description = "Only for results with type 'dataset': the declared size of the dataset")
	private String size; // dataset

	@JsonSchema(description = "Version of the result")
	private String version; // dataset

	@JsonSchema(description = "Geolocation information")
	private List<GeoLocation> geoLocations; // dataset

	@JsonSchema(description = "The OpenAIRE identifiers for this result")
	private String id;

	@JsonSchema(description = "Identifiers of the record at the original sources")
	private List<String> originalIds;

	@JsonSchema(description = "Persistent identifiers of the result")
	private List<ResultPid> pids;

	@JsonSchema(description = "When OpenAIRE collected the record the last time")
	private String dateOfCollection;

	@JsonSchema(description = "Timestamp of last update of the record in OpenAIRE")
	private Long lastUpdateTimeStamp;

	@JsonSchema(description = "Indicators computed for this result, for example UsageCount ones")
	private Indicator indicators;

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public Indicator getIndicators() {
		return indicators;
	}

	public void setIndicators(Indicator indicators) {
		this.indicators = indicators;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public Long getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Long lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getOriginalIds() {
		return originalIds;
	}

	public void setOriginalIds(List<String> originalIds) {
		this.originalIds = originalIds;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<ResultPid> getPids() {
		return pids;
	}

	public void setPids(List<ResultPid> pids) {
		this.pids = pids;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getDateOfCollection() {
		return dateOfCollection;
	}

	public void setDateOfCollection(String dateOfCollection) {
		this.dateOfCollection = dateOfCollection;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<Author> getAuthors() {
		return authors;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<ResultCountry> getCountries() {
		return countries;
	}

	public void setCountries(List<ResultCountry> countries) {
		this.countries = countries;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getMainTitle() {
		return mainTitle;
	}

	public void setMainTitle(String mainTitle) {
		this.mainTitle = mainTitle;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(List<String> descriptions) {
		this.descriptions = descriptions;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getEmbargoEndDate() {
		return embargoEndDate;
	}

	public void setEmbargoEndDate(String embargoEndDate) {
		this.embargoEndDate = embargoEndDate;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getSources() {
		return sources;
	}

	public void setSources(List<String> sources) {
		this.sources = sources;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getFormats() {
		return formats;
	}

	public void setFormats(List<String> formats) {
		this.formats = formats;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getContributors() {
		return contributors;
	}

	public void setContributors(List<String> contributors) {
		this.contributors = contributors;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getCoverages() {
		return coverages;
	}

	public void setCoverages(List<String> coverages) {
		this.coverages = coverages;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public BestAccessRight getBestAccessRight() {
		return bestAccessRight;
	}

	public void setBestAccessRight(BestAccessRight bestAccessRight) {
		this.bestAccessRight = bestAccessRight;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getDocumentationUrls() {
		return documentationUrls;
	}

	public void setDocumentationUrls(List<String> documentationUrls) {
		this.documentationUrls = documentationUrls;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getCodeRepositoryUrl() {
		return codeRepositoryUrl;
	}

	public void setCodeRepositoryUrl(String codeRepositoryUrl) {
		this.codeRepositoryUrl = codeRepositoryUrl;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getProgrammingLanguage() {
		return programmingLanguage;
	}

	public void setProgrammingLanguage(String programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getContactPeople() {
		return contactPeople;
	}

	public void setContactPeople(List<String> contactPeople) {
		this.contactPeople = contactPeople;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getContactGroups() {
		return contactGroups;
	}

	public void setContactGroups(List<String> contactGroups) {
		this.contactGroups = contactGroups;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getTools() {
		return tools;
	}

	public void setTools(List<String> tools) {
		this.tools = tools;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<GeoLocation> getGeoLocations() {
		return geoLocations;
	}

	public void setGeoLocations(List<GeoLocation> geoLocations) {
		this.geoLocations = geoLocations;
	}

}
