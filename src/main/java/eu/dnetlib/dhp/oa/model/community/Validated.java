
package eu.dnetlib.dhp.oa.model.community;

import java.io.Serializable;

/**
 * To store information about the funder funding the project related to the result. It has the following parameters: -
 * shortName of type String to store the funder short name (e.c. AKA). - name of type String to store the funder name
 * (e.c. Akademy of Finland) - fundingStream of type String to store the funding stream - jurisdiction of type String to
 * store the jurisdiction of the funder
 */
public class Validated implements Serializable {

	private String validationDate;
	private Boolean validatedByFunder;

	public void setValidationDate(String validationDate) {
		this.validationDate = validationDate;
	}

	public String getValidationDate() {
		return validationDate;
	}

	public void setValidatedByFunder(Boolean validated) {
		this.validatedByFunder = validated;
	}

	public Boolean getValidatedByFunder() {
		return validatedByFunder;
	}

	public static Validated newInstance(Boolean validated, String validationDate) {
		Validated v = new Validated();
		v.validatedByFunder = validated;
		v.validationDate = validationDate;
		return v;
	}
}
