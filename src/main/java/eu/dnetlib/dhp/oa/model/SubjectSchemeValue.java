
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class SubjectSchemeValue implements Serializable {
	@JsonSchema(
		description = "OpenAIRE subject classification scheme (https://api.openaire.eu/vocabularies/dnet:subject_classification_typologies).")
	private String scheme;

	@JsonSchema(
		description = "The value for the subject in the selected scheme.  When the scheme is 'keyword', it means that the subject is free-text (i.e. not a term from a controlled vocabulary).")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static SubjectSchemeValue newInstance(String scheme, String value) {
		SubjectSchemeValue cf = new SubjectSchemeValue();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}

}
