
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;
import java.util.List;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Container;

/**
 * To store information about the datasource OpenAIRE collects information from. It contains the following parameters: -
 * id of type String to store the OpenAIRE id for the datasource. It corresponds to the parameter id of the datasource
 * represented in the internal model - originalId of type List<String> to store the list of original ids associated to
 * the datasource. It corresponds to the parameter originalId of the datasource represented in the internal model. The
 * null values are filtered out - pid of type List<eu.dnetlib.shp.schema.dump.oaf.ControlledField> to store the
 * persistent identifiers for the datasource. For each pid in the datasource represented in the internal model one pid
 * in the external model is produced as : - schema corresponds to pid.qualifier.classid of the datasource represented in
 * the internal model - value corresponds to pid.value of the datasource represented in the internal model -
 * datasourceType of type eu.dnetlib.dhp.schema.dump.oaf.ControlledField to store the datasource type (e.g.
 * pubsrepository::institutional, Institutional Repository) as in the dnet vocabulary dnet:datasource_typologies. It
 * corresponds to datasourcetype of the datasource represented in the internal model and : - code corresponds to
 * datasourcetype.classid - value corresponds to datasourcetype.classname - openairecompatibility of type String to
 * store information about the OpenAIRE compatibility of the ingested results (which guidelines they are compliant to).
 * It corresponds to openairecompatibility.classname of the datasource represented in the internal model - officialname
 * of type Sgtring to store the official name of the datasource. It correspond to officialname.value of the datasource
 * represented in the internal model - englishname of type String to store the English name of the datasource. It
 * corresponds to englishname.value of the datasource represented in the internal model - websiteurl of type String to
 * store the URL of the website of the datasource. It corresponds to websiteurl.value of the datasource represented in
 * the internal model - logourl of type String to store the URL of the logo for the datasource. It corresponds to
 * logourl.value of the datasource represented in the internal model - dateofvalidation of type String to store the data
 * of validation against the guidelines for the datasource records. It corresponds to dateofvalidation.value of the
 * datasource represented in the internal model - description of type String to store the description for the
 * datasource. It corresponds to description.value of the datasource represented in the internal model
 */
public class Datasource implements Serializable {
	@JsonSchema(description = "The OpenAIRE id of the data source")
	private String id; // string

	@JsonSchema(description = "Original identifiers for the datasource")
	private List<String> originalIds; // list string

	@JsonSchema(description = "Persistent identifiers of the datasource")
	private List<DatasourcePid> pids; // list<String>

	@JsonSchema(
		description = "The type of the datasource. See https://api.openaire.eu/vocabularies/dnet:datasource_typologies")
	private DatasourceSchemeValue type; // value

	@JsonSchema(
		description = "OpenAIRE guidelines the data source comply with. See also https://guidelines.openaire.eu.")
	private String openaireCompatibility; // value

	@JsonSchema(description = "The official name of the datasource")
	private String officialName; // string

	@JsonSchema(description = "The English name of the datasource")
	private String englishName; // string

	private String websiteUrl; // string

	private String logoUrl; // string

	@JsonSchema(description = "The date of last validation against the OpenAIRE guidelines for the datasource records")
	private String dateOfValidation; // string

	private String description; // description

	@JsonSchema(description = "List of subjects associated to the datasource")
	private List<String> subjects; // List<String>

	// opendoar specific fields (od*)

	@JsonSchema(description = "The languages present in the data source's content, as defined by OpenDOAR.")
	private List<String> languages; // odlanguages List<String>

	@JsonSchema(description = "Types of content in the data source, as defined by OpenDOAR")
	private List<String> contentTypes; // odcontent types List<String>

	// re3data fields
	@JsonSchema(description = "Releasing date of the data source, as defined by re3data.org")
	private String releaseStartDate; // string

	@JsonSchema(
		description = "Date when the data source went offline or stopped ingesting new research data. As defined by re3data.org")
	private String releaseEndDate; // string

	@JsonSchema(
		description = "The URL of a mission statement describing the designated community of the data source. As defined by re3data.org")
	private String missionStatementUrl; // string

	@JsonSchema(
		description = "Type of access to the data source, as defined by re3data.org. Possible values: " +
			"{open, restricted, closed}")
	private String accessRights; // databaseaccesstype string

	// {open, restricted or closed}
	@JsonSchema(description = "Type of data upload. As defined by re3data.org: one of {open, restricted,closed}")
	private String uploadRights; // datauploadtype string

	@JsonSchema(
		description = "Access restrinctions to the data source, as defined by re3data.org. One of {feeRequired, registration, other}")
	private String databaseAccessRestriction; // string

	@JsonSchema(
		description = "Upload restrictions applied by the datasource, as defined by re3data.org. One of {feeRequired, registration, other}")
	private String dataUploadRestriction; // string

	@JsonSchema(description = "As defined by redata.org: 'yes' if the data source supports versioning, 'no' otherwise.")
	private Boolean versioning; // boolean

	@JsonSchema(
		description = "The URL of the data source providing information on how to cite its items. As defined by re3data.org.")
	private String citationGuidelineUrl; // string

	// {yes, no, uknown}
	@JsonSchema(
		description = "The persistent identifier system that is used by the data source. As defined by re3data.org")
	private String pidSystems; // string

	@JsonSchema(
		description = "The certificate, seal or standard the data source complies with. As defined by re3data.org.")
	private String certificates; // string

	@JsonSchema(description = "Policies of the data source, as defined in OpenDOAR.")
	private List<String> policies; //

	@JsonSchema(description = "Information about the journal, if this data source is of type Journal.")
	private Container journal; // issn etc del Journal

//	@JsonSchema(description = "Indicators computed for this Datasource, for example UsageCount ones")
//	private Indicator indicators;
//
//	public Indicator getIndicators() {
//		return indicators;
//	}
//
//	public void setIndicators(Indicator indicators) {
//		this.indicators = indicators;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getOriginalIds() {
		return originalIds;
	}

	public void setOriginalIds(List<String> originalIds) {
		this.originalIds = originalIds;
	}

	public List<DatasourcePid> getPids() {
		return pids;
	}

	public void setPids(List<DatasourcePid> pids) {
		this.pids = pids;
	}

	public DatasourceSchemeValue getType() {
		return type;
	}

	public void setType(DatasourceSchemeValue type) {
		this.type = type;
	}

	public String getOpenaireCompatibility() {
		return openaireCompatibility;
	}

	public void setOpenaireCompatibility(String openaireCompatibility) {
		this.openaireCompatibility = openaireCompatibility;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(String officialName) {
		this.officialName = officialName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getDateOfValidation() {
		return dateOfValidation;
	}

	public void setDateOfValidation(String dateOfValidation) {
		this.dateOfValidation = dateOfValidation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	public List<String> getContentTypes() {
		return contentTypes;
	}

	public void setContentTypes(List<String> contentTypes) {
		this.contentTypes = contentTypes;
	}

	public String getReleaseStartDate() {
		return releaseStartDate;
	}

	public void setReleaseStartDate(String releaseStartDate) {
		this.releaseStartDate = releaseStartDate;
	}

	public String getReleaseEndDate() {
		return releaseEndDate;
	}

	public void setReleaseEndDate(String releaseEndDate) {
		this.releaseEndDate = releaseEndDate;
	}

	public String getMissionStatementUrl() {
		return missionStatementUrl;
	}

	public void setMissionStatementUrl(String missionStatementUrl) {
		this.missionStatementUrl = missionStatementUrl;
	}

	public String getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(String accessRights) {
		this.accessRights = accessRights;
	}

	public String getUploadRights() {
		return uploadRights;
	}

	public void setUploadRights(String uploadRights) {
		this.uploadRights = uploadRights;
	}

	public String getDatabaseAccessRestriction() {
		return databaseAccessRestriction;
	}

	public void setDatabaseAccessRestriction(String databaseAccessRestriction) {
		this.databaseAccessRestriction = databaseAccessRestriction;
	}

	public String getDataUploadRestriction() {
		return dataUploadRestriction;
	}

	public void setDataUploadRestriction(String dataUploadRestriction) {
		this.dataUploadRestriction = dataUploadRestriction;
	}

	public Boolean getVersioning() {
		return versioning;
	}

	public void setVersioning(Boolean versioning) {
		this.versioning = versioning;
	}

	public String getCitationGuidelineUrl() {
		return citationGuidelineUrl;
	}

	public void setCitationGuidelineUrl(String citationGuidelineUrl) {
		this.citationGuidelineUrl = citationGuidelineUrl;
	}

	public String getPidSystems() {
		return pidSystems;
	}

	public void setPidSystems(String pidSystems) {
		this.pidSystems = pidSystems;
	}

	public String getCertificates() {
		return certificates;
	}

	public void setCertificates(String certificates) {
		this.certificates = certificates;
	}

	public List<String> getPolicies() {
		return policies;
	}

	public void setPolicies(List<String> policiesr3) {
		this.policies = policiesr3;
	}

	public Container getJournal() {
		return journal;
	}

	public void setJournal(Container journal) {
		this.journal = journal;
	}
}
