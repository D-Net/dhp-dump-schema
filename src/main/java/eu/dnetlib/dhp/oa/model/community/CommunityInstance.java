
package eu.dnetlib.dhp.oa.model.community;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Instance;

/**
 * It extends eu.dnetlib.dhp.dump.oaf.Instance with values related to the community dump. In the Result dump this
 * information is not present because it is dumped as a set of relations between the result and the datasource. -
 * hostedby of type eu.dnetlib.dhp.schema.dump.oaf.KeyValue to store the information about the source from which the
 * instance can be viewed or downloaded. It is mapped against the hostedby parameter of the instance to be dumped and -
 * key corresponds to hostedby.key - value corresponds to hostedby.value - collectedfrom of type
 * eu.dnetlib.dhp.schema.dump.oaf.KeyValue to store the information about the source from which the instance has been
 * collected. It is mapped against the collectedfrom parameter of the instance to be dumped and - key corresponds to
 * collectedfrom.key - value corresponds to collectedfrom.value
 */
public class CommunityInstance extends Instance {
	@JsonSchema(description = "Information about the source from which the instance can be viewed or downloaded.")
	private CfHbKeyValue hostedBy;

	@JsonSchema(description = "Information about the source from which the record has been collected")
	private CfHbKeyValue collectedFrom;

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public CfHbKeyValue getHostedBy() {
		return hostedBy;
	}

	public void setHostedBy(CfHbKeyValue hostedBy) {
		this.hostedBy = hostedBy;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public CfHbKeyValue getCollectedFrom() {
		return collectedFrom;
	}

	public void setCollectedFrom(CfHbKeyValue collectedFrom) {
		this.collectedFrom = collectedFrom;
	}
}
