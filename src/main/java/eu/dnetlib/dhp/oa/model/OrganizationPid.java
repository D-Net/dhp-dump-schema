
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public

class OrganizationPid implements Serializable {
	@JsonSchema(description = "The scheme of the identifier (i.e. isni)")
	private String scheme;

	@JsonSchema(description = "The value in the schema (i.e. 0000000090326370)")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static OrganizationPid newInstance(String scheme, String value) {
		OrganizationPid cf = new OrganizationPid();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}

}
