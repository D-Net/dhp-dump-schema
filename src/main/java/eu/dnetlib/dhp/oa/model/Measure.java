
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * @author miriam.baglioni
 * @Date 03/08/22
 */
public class Measure implements Serializable {
	@JsonSchema(description = "The measure (i.e. class)")
	private String key;

	@JsonSchema(description = "The value for that measure")
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static Measure newInstance(String key, String value) {
		Measure mes = new Measure();
		mes.key = key;
		mes.value = value;
		return mes;
	}

	@JsonIgnore
	public boolean isBlank() {
		return StringUtils.isBlank(key) && StringUtils.isBlank(value);
	}
}
