package eu.dnetlib.dhp.oa.model.community;

import java.io.Serializable;
import java.util.List;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.OrganizationPid;

public class Affiliation implements Serializable {
    private String legalName;
    @JsonSchema(description = "The OpenAIRE id for the organisation")
    private String id;
    @JsonSchema(description = "Persistent identifiers for the organisation i.e. isni 0000000090326370")
    private List<OrganizationPid> pids;

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<OrganizationPid> getPids() {
        return pids;
    }

    public void setPids(List<OrganizationPid> pids) {
        this.pids = pids;
    }
}
