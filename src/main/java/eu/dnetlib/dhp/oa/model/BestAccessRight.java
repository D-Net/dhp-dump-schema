
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * BestAccessRight. Used to represent the result best access rights. Values for this element are found against the
 * COAR access right scheme. The classid of the element accessright in eu.dnetlib.dhp.schema.oaf.Result is used to get
 * the COAR corresponding code whose value will be used to set the code parameter. The COAR label corresponding to the
 * COAR code will be used to set the label parameter. The scheme value will always be the one referring to the COAR
 * access right scheme
 */

public class BestAccessRight implements Serializable {

	@JsonSchema(
		description = "COAR access mode code: http://vocabularies.coar-repositories.org/documentation/access_rights/")
	private String code; // the classid in the Qualifier

	@JsonSchema(description = "Label for the access mode")
	private String label; // the classname in the Qualifier

	@JsonSchema(
		description = "Scheme of reference for access right code. Always set to COAR access rights vocabulary: http://vocabularies.coar-repositories.org/documentation/access_rights/")
	private String scheme;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static BestAccessRight newInstance(String code, String label, String scheme) {
		BestAccessRight ar = new BestAccessRight();
		ar.code = code;
		ar.label = label;
		ar.scheme = scheme;
		return ar;
	}
}
