
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To describe the funded amount. It has the following parameters: - private String currency to store the currency of
 * the fund - private float totalcost to store the total cost of the project - private float fundedamount to store the
 * funded amount by the funder
 */
public class Granted implements Serializable {
	@JsonSchema(description = "The currency of the granted amount (e.g. EUR)")
	private String currency;

	@JsonSchema(description = "The total cost of the project")
	private float totalCost;

	@JsonSchema(description = "The funded amount")
	private float fundedAmount;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}

	public float getFundedAmount() {
		return fundedAmount;
	}

	public void setFundedAmount(float fundedAmount) {
		this.fundedAmount = fundedAmount;
	}

	public static Granted newInstance(String currency, float totalcost, float fundedamount) {
		Granted granted = new Granted();
		granted.currency = currency;
		granted.totalCost = totalcost;
		granted.fundedAmount = fundedamount;
		return granted;
	}

	public static Granted newInstance(String currency, float fundedamount) {
		Granted granted = new Granted();
		granted.currency = currency;
		granted.fundedAmount = fundedamount;
		return granted;
	}
}
