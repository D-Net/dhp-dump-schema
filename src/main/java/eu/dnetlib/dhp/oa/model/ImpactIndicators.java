
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

/**
 * @author miriam.baglioni
 * @Date 07/11/22
 */
public class ImpactIndicators implements Serializable {
	Score influence;
	Score influenceAlt;
	Score popularity;
	Score popularityAlt;
	Score impulse;

	public Score getInfluence() {
		return influence;
	}

	public void setInfluence(Score influence) {
		this.influence = influence;
	}

	public Score getInfluenceAlt() {
		return influenceAlt;
	}

	public void setInfluenceAlt(Score influenceAlt) {
		this.influenceAlt = influenceAlt;
	}

	public Score getPopularity() {
		return popularity;
	}

	public void setPopularity(Score popularity) {
		this.popularity = popularity;
	}

	public Score getPopularityAlt() {
		return popularityAlt;
	}

	public void setPopularityAlt(Score popularityAlt) {
		this.popularityAlt = popularityAlt;
	}

	public Score getImpulse() {
		return impulse;
	}

	public void setImpulse(Score impulse) {
		this.impulse = impulse;
	}
}
