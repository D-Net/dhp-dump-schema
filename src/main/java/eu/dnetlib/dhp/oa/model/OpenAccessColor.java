
package eu.dnetlib.dhp.oa.model;

/**
 * @author miriam.baglioni
 * @Date 19/12/23
 */
/**
 * The OpenAccess color meant to be used on the result level
 */
public enum OpenAccessColor {

	gold, hybrid, bronze

}
