
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class Language implements Serializable {

	@JsonSchema(description = "alpha-3/ISO 639-2 code of the language")
	private String code; // the classid in the Qualifier

	@JsonSchema(description = "Language label in English")
	private String label; // the classname in the Qualifier

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static Language newInstance(String code, String value) {
		Language qualifier = new Language();
		qualifier.setCode(code);
		qualifier.setLabel(value);
		return qualifier;
	}
}
