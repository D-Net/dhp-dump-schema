
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public // TODO change the DatasourceSchemaValue to DatasourceKeyValue. The scheme is always the dnet one. What we show
		// here is the entry in the scheme (the key) and its understandable value
class DatasourceSchemeValue implements Serializable {
	@JsonSchema(description = "The scheme used to express the value (i.e. pubsrepository::journal)")
	private String scheme;

	@JsonSchema(description = "The value expressed in the scheme (Journal)")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static DatasourceSchemeValue newInstance(String scheme, String value) {
		DatasourceSchemeValue cf = new DatasourceSchemeValue();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}
}
