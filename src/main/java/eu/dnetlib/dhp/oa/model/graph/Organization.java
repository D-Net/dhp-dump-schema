
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;
import java.util.List;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Country;
import eu.dnetlib.dhp.oa.model.OrganizationPid;

/**
 * To represent the generic organizaiton. It has the following parameters:
 * - private String legalshortname to store the legalshortname of the organizaiton
 * - private String legalname to store the legal name of the organization
 * - private String websiteurl to store the websiteurl of the organization
 * - private List<String> alternativenames to store the alternative names of the organization
 * - private Country country to store the country of the organization
 * - private String id to store the openaire id of the organization
 * - private List<OrganizationPid> pid to store the list of pids for the organization
 */
public class Organization implements Serializable {
	private String legalShortName;
	private String legalName;
	private String websiteUrl;

	@JsonSchema(description = "Alternative names that identify the organisation")
	private List<String> alternativeNames;

	@JsonSchema(description = "The organisation country")
	private Country country;

	@JsonSchema(description = "The OpenAIRE id for the organisation")
	private String id;

	@JsonSchema(description = "Persistent identifiers for the organisation i.e. isni 0000000090326370")
	private List<OrganizationPid> pids;

	public String getLegalShortName() {
		return legalShortName;
	}

	public void setLegalShortName(String legalShortName) {
		this.legalShortName = legalShortName;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public List<String> getAlternativeNames() {
		return alternativeNames;
	}

	public void setAlternativeNames(List<String> alternativeNames) {
		this.alternativeNames = alternativeNames;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<OrganizationPid> getPids() {
		return pids;
	}

	public void setPids(List<OrganizationPid> pids) {
		this.pids = pids;
	}

}
