
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class Indicator implements Serializable {
	@JsonSchema(description = "The impact measures (i.e. popularity)")
	BipIndicators citationImpact;

	@JsonSchema(description = "The usage counts (i.e. downloads)")
	UsageCounts usageCounts;

	public BipIndicators getCitationImpact() {
		return citationImpact;
	}

	public void setCitationImpact(BipIndicators citationImpact) {
		this.citationImpact = citationImpact;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public UsageCounts getUsageCounts() {
		return usageCounts;
	}

	public void setUsageCounts(UsageCounts usageCounts) {
		this.usageCounts = usageCounts;
	}
}
