
package eu.dnetlib.dhp.oa.model;

/**
 * AccessRight. Used to represent the result access rights. It extends the eu.dnet.lib.dhp.schema.dump.oaf.BestAccessRight
 * element with value for the openaccess route
 */
public class AccessRight extends BestAccessRight {

	private OpenAccessRoute openAccessRoute;

	public static AccessRight newInstance(String code, String label, String scheme) {
		AccessRight ar = new AccessRight();
		ar.setCode(code);
		ar.setLabel(label);
		ar.setScheme(scheme);
		return ar;
	}

	public OpenAccessRoute getOpenAccessRoute() {
		return openAccessRoute;
	}

	public void setOpenAccessRoute(OpenAccessRoute openAccessRoute) {
		this.openAccessRoute = openAccessRoute;
	}
}
