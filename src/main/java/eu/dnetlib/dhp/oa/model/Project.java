
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * This class to store the common information about the project that will be dumped for community and for the whole
 * graph - private String id to store the id of the project (OpenAIRE id) - private String code to store the grant
 * agreement of the project - private String acronym to store the acronym of the project - private String title to store
 * the tile of the project
 */
public class Project implements Serializable {
	@JsonSchema(description = "The OpenAIRE id for the project")
	protected String id;// OpenAIRE id

	@JsonSchema(description = "The grant agreement number")
	protected String code;

	@JsonSchema(description = "The acronym of the project")
	protected String acronym;

	protected String title;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
