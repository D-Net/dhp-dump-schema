
package eu.dnetlib.dhp.oa.model.graph;

import java.util.List;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Instance;
import eu.dnetlib.dhp.oa.model.Result;

/**
 * It extends the eu.dnetlib.dhp.schema.dump.oaf.Result with - instance of type
 * List<eu.dnetlib.dhp.schema.dump.oaf.Instance> to store all the instances associated to the result. It corresponds to
 * the same parameter in the result represented in the internal model
 */
public class GraphResult extends Result {
	@JsonSchema(
		description = "Each instance is one specific materialisation or version of the result. For example, you can have one result with three instance: one is the pre-print, one is the post-print, one is te published version")
	private List<Instance> instances;

	public List<Instance> getInstances() {
		return instances;
	}

	public void setInstances(List<Instance> instances) {
		this.instances = instances;
	}
}
