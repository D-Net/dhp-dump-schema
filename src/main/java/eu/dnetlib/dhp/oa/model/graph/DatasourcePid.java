
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class DatasourcePid implements Serializable {

	@JsonSchema(description = "The scheme used to express the value ")
	private String scheme;

	@JsonSchema(description = "The value expressed in the scheme ")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static DatasourcePid newInstance(String scheme, String value) {
		DatasourcePid cf = new DatasourcePid();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}

}
