
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class AlternateIdentifier implements Serializable {
	@JsonSchema(
		description = "The scheme of the identifier. It can be a persistent identifier (i.e. doi). If it is present in the alternate identifiers "
			+
			"it means it has not been forged by an authority for that pid. For example we collect metadata from an institutional repository that provides "
			+
			"as identifier for the result also the doi")
	private String scheme;

	@JsonSchema(description = "The value expressed in the scheme")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static AlternateIdentifier newInstance(String scheme, String value) {
		AlternateIdentifier cf = new AlternateIdentifier();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}
}
