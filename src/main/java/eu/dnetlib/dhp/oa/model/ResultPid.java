
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class ResultPid implements Serializable {
	@JsonSchema(
		description = "The scheme of the persistent identifier for the result (i.e. doi). " +
			"If the pid is here it means the information for the pid has been collected from an authority for " +
			"that pid type (i.e. Crossref/Datacite for doi). The set of authoritative pid is: doi when collected from Crossref or Datacite "
			+
			"pmid when collected from EuroPubmed, arxiv when collected from arXiv, handle from the repositories")
	private String scheme;

	@JsonSchema(description = "The value expressed in the scheme (i.e. 10.1000/182)")
	private String value;

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static ResultPid newInstance(String scheme, String value) {
		ResultPid cf = new ResultPid();

		cf.setScheme(scheme);
		cf.setValue(value);

		return cf;
	}
}
