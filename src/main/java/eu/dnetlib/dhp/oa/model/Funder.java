
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class Funder implements Serializable {

	@JsonSchema(description = "The short name of the funder (EC)")
	private String shortName;

	@JsonSchema(description = "The name of the funder (European Commission)")
	private String name;

	@JsonSchema(
		description = "Geographical jurisdiction (e.g. for European Commission is EU, for Croatian Science Foundation is HR)")
	private String jurisdiction;

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
