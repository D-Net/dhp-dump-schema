
package eu.dnetlib.dhp.oa.model.graph;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To store information about the funder funding the project related to the result. It extends
 * eu.dnetlib.dhp.schema.dump.oaf.Funder with the following parameter: - - private
 * eu.dnetdlib.dhp.schema.dump.oaf.graph.Fundings funding_stream to store the fundingstream
 */
public class Funder extends eu.dnetlib.dhp.oa.model.Funder {

	@JsonSchema(description = "Description of the funding stream")
	private Fundings fundingStream;

	public Fundings getFundingStream() {
		return fundingStream;
	}

	public void setFundingStream(Fundings fundingStream) {
		this.fundingStream = fundingStream;
	}
}
