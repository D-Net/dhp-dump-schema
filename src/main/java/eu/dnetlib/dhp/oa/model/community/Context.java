
package eu.dnetlib.dhp.oa.model.community;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Provenance;

/**
 * Reference to a relevant research infrastructure, initiative or community (RI/RC) among those collaborating with
 * OpenAIRE. It extend eu.dnetlib.dhp.shema.dump.oaf.Qualifier with a parameter provenance of type
 * List<eu.dnetlib.dhp.schema.dump.oaf.Provenance> to store the provenances of the association between the result and
 * the RC/RI. The values for this element correspond to: - code: it corresponds to the id of the context in the result
 * to be mapped. If the context id refers to a RC/RI and contains '::' only the part of the id before the first "::"
 * will be used as value for code - label it corresponds to the label associated to the id. The information id taken
 * from the profile of the RC/RI - provenance it is set only if the dataInfo associated to the contenxt element of the
 * result to be dumped is not null. For each dataInfo one instance of type eu.dnetlib.dhp.schema.dump.oaf.Provenance is
 * instantiated if the element datainfo.provenanceaction is not null. In this case - provenance corresponds to
 * dataInfo.provenanceaction.classname - trust corresponds to dataInfo.trust
 */
public class Context {
	@JsonSchema(description = "Code identifying the RI/RC")
	private String code;

	@JsonSchema(description = "Label of the RI/RC")
	private String label;

	@JsonSchema(description = "Why this result is associated to the RI/RC.")
	private List<Provenance> provenance;

	public List<Provenance> getProvenance() {
		return provenance;
	}

	public void setProvenance(List<Provenance> provenance) {
		this.provenance = provenance;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public int hashCode() {
		final String p = Optional
			.ofNullable(getProvenance())
			.map(
				prov -> prov
					.stream()
					.map(Provenance::toString)
					.collect(Collectors.joining()))
			.orElse("");

		return Objects.hash(getCode(), getLabel(), p);
	}

}
