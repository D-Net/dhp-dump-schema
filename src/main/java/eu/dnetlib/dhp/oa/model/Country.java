
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * Represents the country associated to the generic entity. It extends eu.dnetlib.dhp.schema.dump.oaf.Qualifier with a
 * provenance parameter of type eu.dnetlib.dhp.schema.dumo.oaf.Provenance. The country in not mapped if its value in the
 * result reprensented in the internal format is Unknown. The value for this element correspond to: - code corresponds
 * to the classid of eu.dnetlib.dhp.schema.oaf.Country - label corresponds to the classname of
 * eu.dnetlib.dhp.schema.oaf.Country - provenance set only if the dataInfo associated to the Country of the result to be
 * dumped is not null. In this case : - provenance corresponds to dataInfo.provenanceaction.classid (to be modified with
 * datainfo.provenanceaction.classname) - trust corresponds to dataInfo.trust
 */
public class Country implements Serializable {
	@JsonSchema(description = "ISO 3166-1 alpha-2 country code (i.e. IT)")
	private String code; // the classid in the Qualifier

	@JsonSchema(description = "The label for that code (i.e. Italy)")
	private String label; // the classname in the Qualifier

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static Country newInstance(String code, String label) {
		Country c = new Country();
		c.setCode(code);
		c.setLabel(label);
		return c;
	}

}
