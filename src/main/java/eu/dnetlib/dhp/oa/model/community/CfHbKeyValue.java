
package eu.dnetlib.dhp.oa.model.community;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

public class CfHbKeyValue implements Serializable {

	@JsonSchema(description = "the OpenAIRE identifier of the data source")
	private String key;

	@JsonSchema(description = "the name of the data source")
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static CfHbKeyValue newInstance(String key, String value) {
		CfHbKeyValue inst = new CfHbKeyValue();
		inst.key = key;
		inst.value = value;
		return inst;
	}

	@JsonIgnore
	public boolean isBlank() {
		return StringUtils.isBlank(key) && StringUtils.isBlank(value);
	}

}
