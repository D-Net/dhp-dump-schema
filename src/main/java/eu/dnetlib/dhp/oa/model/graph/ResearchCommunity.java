
package eu.dnetlib.dhp.oa.model.graph;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To represent RC entities. It extends eu.dnetlib.dhp.dump.oaf.grap.ResearchInitiative by adding the parameter subject
 * to store the list of subjects related to the community
 */
public class ResearchCommunity extends ResearchInitiative {
	@JsonSchema(
		description = "Only for research communities: the list of the subjects associated to the research community")

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<String> subjects;

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}
}
