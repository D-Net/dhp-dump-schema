
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

/**
 * @author miriam.baglioni
 * @Date 07/11/22
 */
public class UsageCounts implements Serializable {
	private Integer downloads;
	private Integer views;

	public Integer getDownloads() {
		return downloads;
	}

	public void setDownloads(Integer downloads) {
		this.downloads = downloads;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}
}
