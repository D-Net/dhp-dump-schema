
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * Represents the manifestations (i.e. different versions) of the result. For example: the pre-print and the published
 * versions are two manifestations of the same research result. It has the following parameters: - license of type
 * String to store the license applied to the instance. It corresponds to the value of the licence in the instance to be
 * dumped - accessright of type eu.dnetlib.dhp.schema.dump.oaf.AccessRight to store the accessright of the instance. -
 * type of type String to store the type of the instance as defined in the corresponding dnet vocabulary
 * (dnet:pubication_resource). It corresponds to the instancetype.classname of the instance to be mapped - url of type
 * List<String> list of locations where the instance is accessible. It corresponds to url of the instance to be dumped -
 * publicationdate of type String to store the publication date of the instance ;// dateofacceptance; - refereed of type
 * String to store information abour the review status of the instance. Possible values are 'Unknown',
 * 'nonPeerReviewed', 'peerReviewed'. It corresponds to refereed.classname of the instance to be dumped
 * - articleprocessingcharge of type APC to store the article processing charges possibly associated to the instance
 * -pid of type List<ControlledField> that is the list of pids associated to the result coming from authoritative sources for that pid
 * -alternateIdentifier of type List<ControlledField> that is the list of pids associated to the result coming from NON authoritative
 * sources for that pid
 * -measure list<KeyValue> to represent the measure computed for this instance (for example the Bip!Finder ones). It corresponds to measures in the model
 */
public class Instance implements Serializable {

//	@JsonSchema(description = "Indicators computed for this instance, for example Bip!Finder ones")
//	private Indicator indicators;

	private List<ResultPid> pids;

	@JsonSchema(
		description = "All the identifiers other than pids forged by an authorithy for the pid type (i.e. Crossref for DOIs")
	private List<AlternateIdentifier> alternateIdentifiers;

	private String license;

	@JsonSchema(description = "The accessRights for this materialization of the result")
	private AccessRight accessRight;

	@JsonSchema(
		description = "The specific sub-type of this instance (see https://api.openaire.eu/vocabularies/dnet:result_typologies following the links)")
	private String type;

	@JsonSchema(
		description = "URLs to the instance. They may link to the actual full-text or to the landing page at the hosting source. ")
	private List<String> urls;

	@JsonSchema(
		description = "The money spent to make this book or article available in Open Access. Source for this information is the OpenAPC initiative.")
	private APC articleProcessingCharge;

	@JsonSchema(description = "Date of the research product")
	private String publicationDate;// dateofacceptance;

	@JsonSchema(
		description = "If this instance has been peer-reviewed or not. Allowed values are peerReviewed, " +
			"nonPeerReviewed, UNKNOWN (as defined in https://api.openaire.eu/vocabularies/dnet:review_levels)")
	private String refereed; // peer-review status

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public AccessRight getAccessRight() {
		return accessRight;
	}

	public void setAccessRight(AccessRight accessRight) {
		this.accessRight = accessRight;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<String> getUrls() {
		return urls;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String getRefereed() {
		return refereed;
	}

	public void setRefereed(String refereed) {
		this.refereed = refereed;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public APC getArticleProcessingCharge() {
		return articleProcessingCharge;
	}

	public void setArticleProcessingCharge(APC articleProcessingCharge) {
		this.articleProcessingCharge = articleProcessingCharge;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<ResultPid> getPids() {
		return pids;
	}

	public void setPids(List<ResultPid> pids) {
		this.pids = pids;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<AlternateIdentifier> getAlternateIdentifiers() {
		return alternateIdentifiers;
	}

	public void setAlternateIdentifiers(List<AlternateIdentifier> alternateIdentifiers) {
		this.alternateIdentifiers = alternateIdentifiers;
	}

//	@JsonInclude(JsonInclude.Include.NON_NULL)
//	public Indicator getIndicators() {
//		return indicators;
//	}
//
//	public void setIndicators(Indicator indicators) {
//		this.indicators = indicators;
//	}
}
