
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To represent the generic persistent identifier. It has two parameters:
 * - id of type
 * eu.dnetlib.dhp.schema.dump.oaf.AuthorPidSchemeValue to store the scheme and value of the Persistent Identifier.
 * - provenance of type eu.dnetlib.dhp.schema.dump.oaf.Provenance to store the provenance and trust of the information
 */
public class AuthorPid implements Serializable {

	private AuthorPidSchemeValue id;

	@JsonSchema(description = "The reason why the pid was associated to the author")
	private Provenance provenance;

	public AuthorPidSchemeValue getId() {
		return id;
	}

	public void setId(AuthorPidSchemeValue pid) {
		this.id = pid;
	}

	public Provenance getProvenance() {
		return provenance;
	}

	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}

	public static AuthorPid newInstance(AuthorPidSchemeValue pid, Provenance provenance) {
		AuthorPid p = new AuthorPid();
		p.id = pid;
		p.provenance = provenance;

		return p;
	}

	public static AuthorPid newInstance(AuthorPidSchemeValue pid) {
		AuthorPid p = new AuthorPid();
		p.id = pid;

		return p;
	}

}
