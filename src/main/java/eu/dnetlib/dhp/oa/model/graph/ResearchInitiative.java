
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To represent entity of type RC/RI. It has the following parameters, which are mostly derived by the profile
 * - private
 * String id to store the openaire id for the entity. Is has as code 00 and will be created as
 * 00|context_____::md5(originalId) private
 * String originalId to store the id of the context as provided in the profile
 * (i.e. mes)
 * - private String name to store the name of the context (got from the label attribute in the context
 * definition)
 * - private String type to store the type of the context (i.e.: research initiative or research community)
 * - private String description to store the description of the context as given in the profile
 * -private String
 * zenodo_community to store the zenodo community associated to the context (main zenodo community)
 */
public class ResearchInitiative implements Serializable {
	@JsonSchema(description = "The OpenAIRE id for the community/research infrastructure")
	private String id; // openaireId

	@JsonSchema(description = "The acronym of the community")
	private String acronym; // context id

	@JsonSchema(description = "The long name of the community")
	private String name; // context name

	@JsonSchema(description = "One of {Research Community, Research infrastructure}")
	private String type; // context type: research initiative or research community

	@JsonSchema(description = "Description of the research community/research infrastructure")
	private String description;

	@JsonSchema(
		description = "The URL of the Zenodo community associated to the Research community/Research infrastructure")
	private String zenodoCommunity;

	public String getZenodoCommunity() {
		return zenodoCommunity;
	}

	public void setZenodoCommunity(String zenodoCommunity) {
		this.zenodoCommunity = zenodoCommunity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String label) {
		this.name = label;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
