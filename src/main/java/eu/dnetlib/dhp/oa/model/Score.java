
package eu.dnetlib.dhp.oa.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * @author miriam.baglioni
 * @Date 07/11/22
 */
public class Score implements Serializable {
	private String indicator;
	private String score;

	@JsonProperty("class")
	private String clazz;

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	@JsonGetter("class")
	public String getClazz() {
		return clazz;
	}

	@JsonSetter("class")
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}
}
