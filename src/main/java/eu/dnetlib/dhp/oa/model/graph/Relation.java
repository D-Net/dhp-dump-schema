
package eu.dnetlib.dhp.oa.model.graph;

import java.io.Serializable;
import java.util.Objects;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Provenance;

/**
 * To represent the gereric relation between two entities. It has the following parameters: - private Node source to
 * represent the entity source of the relation - private Node target to represent the entity target of the relation -
 * private RelType reltype to represent the semantics of the relation - private Provenance provenance to represent the
 * provenance of the relation
 */
public class Relation implements Serializable {
	@JsonSchema(description = "The identifier of the source in the relation")
	private String source;

	@JsonSchema(description = "The entity type of the source in the relation")
	private String sourceType;

	@JsonSchema(description = "The identifier of the target in the relation")
	private String target;

	@JsonSchema(description = "The entity type of the target in the relation")
	private String targetType;

	@JsonSchema(description = "To represent the semantics of a relation between two entities")
	private RelType relType;

	@JsonSchema(description = "The reason why OpenAIRE holds the relation ")
	private Provenance provenance;

	@JsonSchema(
		description = "True if the relation is related to a project and it has been collected from an authoritative source (i.e. the funder)")
	private boolean validated;

	@JsonSchema(description = "The date when the relation was collected from OpenAIRE")
	private String validationDate;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public RelType getRelType() {
		return relType;
	}

	public void setRelType(RelType relType) {
		this.relType = relType;
	}

	public Provenance getProvenance() {
		return provenance;
	}

	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}

	public void setValidated(boolean validate) {
		this.validated = validate;
	}

	public boolean getValidated() {
		return validated;
	}

	public void setValidationDate(String validationDate) {
		this.validationDate = validationDate;
	}

	public String getValidationDate() {
		return validationDate;
	}

	@Override
	public int hashCode() {

		return Objects.hash(source, target, relType.getType() + ":" + relType.getName());
	}

	public static Relation newInstance(String source, String sourceType, String target, String targetType,
		RelType reltype, Provenance provenance) {
		Relation relation = new Relation();
		relation.source = source;
		relation.sourceType = sourceType;
		relation.target = target;
		relation.targetType = targetType;
		relation.relType = reltype;
		relation.provenance = provenance;
		return relation;
	}
}
