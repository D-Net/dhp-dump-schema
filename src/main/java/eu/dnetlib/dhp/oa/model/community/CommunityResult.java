
package eu.dnetlib.dhp.oa.model.community;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

import eu.dnetlib.dhp.oa.model.Result;

/**
 * extends eu.dnetlib.dhp.schema.dump.oaf.Result with the following parameters: - projects of type
 * List<eu.dnetlib.dhp.schema.dump.oaf.community.Project> to store the list of projects related to the result. The
 * information is added after the result is mapped to the external model - context of type
 * List<eu.dnetlib.dhp.schema.dump.oaf.community.Context> to store information about the RC RI related to the result.
 * For each context in the result represented in the internal model one context in the external model is produced -
 * collectedfrom of type List<eu.dnetliv.dhp.schema.dump.oaf.KeyValue> to store information about the sources from which
 * the record has been collected. For each collectedfrom in the result represented in the internal model one
 * collectedfrom in the external model is produced - instance of type
 * List<eu.dnetlib.dhp.schema.dump.oaf.community.CommunityInstance> to store all the instances associated to the result.
 * It corresponds to the same parameter in the result represented in the internal model
 */
public class CommunityResult extends Result {

	@JsonSchema(description = "List of projects (i.e. grants) that (co-)funded the production ofn the research results")
	private List<Project> projects;

	@JsonSchema(description = "List of organizations in an affiliation relation with the research results")
	private List<Affiliation> organizations;


	@JsonSchema(
		description = "Reference to a relevant research infrastructure, initiative or community (RI/RC) among those collaborating with OpenAIRE. Please see https://connect.openaire.eu")
	private List<Context> communities;

	@JsonSchema(description = "Information about the sources from which the record has been collected")
	protected List<CfHbKeyValue> collectedFrom;

	@JsonSchema(
		description = "Each instance is one specific materialisation or version of the result. For example, you can have one result with three instance: one is the pre-print, one is the post-print, one is te published version")
	private List<CommunityInstance> instances;

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<CommunityInstance> getInstances() {
		return instances;
	}

	public void setInstances(List<CommunityInstance> instances) {
		this.instances = instances;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<CfHbKeyValue> getCollectedFrom() {
		return collectedFrom;
	}

	public void setCollectedFrom(List<CfHbKeyValue> collectedFrom) {
		this.collectedFrom = collectedFrom;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	public List<Context> getCommunities() {
		return communities;
	}

	public void setCommunities(List<Context> communities) {
		this.communities = communities;
	}

	public List<Affiliation> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<Affiliation> organizations) {
		this.organizations = organizations;
	}
}
