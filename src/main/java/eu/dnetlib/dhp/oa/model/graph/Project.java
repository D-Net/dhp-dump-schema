
package eu.dnetlib.dhp.oa.model.graph;

import java.util.List;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * This is the class representing the Project in the model used for the dumps of the whole graph. At the moment the dump
 * of the Projects differs from the other dumps because we do not create relations between Funders (Organization) and
 * Projects but we put the information about the Funder within the Project representation. We also removed the
 * collected from element from the Project. No relation between the Project and the Datasource entity from which it is
 * collected will be created. We will never create relations between Project and Datasource. In case some relation will
 * be extracted from the Project they will refer the Funder and will be of type ( organization -> funds -> project,
 * project -> isFundedBy -> organization) We also removed the duration parameter because the most of times it is set to
 * 0. It has the following parameters:
 * - private String id to store the id of the project (OpenAIRE id)
 * - private String websiteurl to store the websiteurl of the project
 * - private String code to store the grant agreement of the project
 * - private String acronym to store the acronym of the project
 * - private String title to store the tile of the project
 * - private String startdate to store the start date
 * - private String enddate to store the end date
 * - private String callidentifier to store the call indentifier
 * - private String keywords to store the keywords
 * - private boolean openaccessmandateforpublications to store if the project must accomplish to the open access mandate
 *   for publications. This value will be set to true if one of the field in the project represented in the internal model
 *   is set to true
 * - private boolean openaccessmandatefordataset to store if the project must accomplish to the open access mandate for
 * 	 dataset. It is set to the value in the corresponding filed of the project represented in the internal model
 * - private List<String> subject to store the list of subjects of the project
 * - private List<Funder> funding to store the list of funder of the project
 * - private String summary to store the summary of the project
 * - private Granted granted to store the granted amount
 * - private List<Programme> h2020programme to store the list of programmes the project is related to
 */

public class Project extends eu.dnetlib.dhp.oa.model.Project   {
	private String websiteUrl;
	private String startDate;

	private String endDate;

	private String callIdentifier;

	private String keywords;

	private boolean openAccessMandateForPublications;

	private boolean openAccessMandateForDataset;
	private List<String> subjects;

	@JsonSchema(description = "Funding information for the project")
	private List<Funder> fundings;

	private String summary;

	@JsonSchema(description = "The money granted to the project")
	private Granted granted;

	@JsonSchema(description = "The h2020 programme funding the project")
	private List<Programme> h2020Programmes;


	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCallIdentifier() {
		return callIdentifier;
	}

	public void setCallIdentifier(String callIdentifier) {
		this.callIdentifier = callIdentifier;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public boolean isOpenAccessMandateForPublications() {
		return openAccessMandateForPublications;
	}

	public void setOpenAccessMandateForPublications(boolean openAccessMandateForPublications) {
		this.openAccessMandateForPublications = openAccessMandateForPublications;
	}

	public boolean isOpenAccessMandateForDataset() {
		return openAccessMandateForDataset;
	}

	public void setOpenAccessMandateForDataset(boolean openAccessMandateForDataset) {
		this.openAccessMandateForDataset = openAccessMandateForDataset;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public List<Funder> getFundings() {
		return fundings;
	}

	public void setFundings(List<Funder> fundings) {
		this.fundings = fundings;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Granted getGranted() {
		return granted;
	}

	public void setGranted(Granted granted) {
		this.granted = granted;
	}

	public List<Programme> getH2020Programmes() {
		return h2020Programmes;
	}

	public void setH2020Programmes(List<Programme> h2020Programmes) {
		this.h2020Programmes = h2020Programmes;
	}
}
