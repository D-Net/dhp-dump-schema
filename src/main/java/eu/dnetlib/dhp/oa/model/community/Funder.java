
package eu.dnetlib.dhp.oa.model.community;

import com.github.imifou.jsonschema.module.addon.annotation.JsonSchema;

/**
 * To store information about the funder funding the project related to the result. It has the following parameters: -
 * shortName of type String to store the funder short name (e.c. AKA). - name of type String to store the funder name
 * (e.c. Akademy of Finland) - fundingStream of type String to store the funding stream - jurisdiction of type String to
 * store the jurisdiction of the funder
 */
public class Funder extends eu.dnetlib.dhp.oa.model.Funder {

	@JsonSchema(description = "Stream of funding (e.g. for European Commission can be H2020 or FP7)")
	private String fundingStream;

	public String getFundingStream() {
		return fundingStream;
	}

	public void setFundingStream(String fundingStream) {
		this.fundingStream = fundingStream;
	}
}
